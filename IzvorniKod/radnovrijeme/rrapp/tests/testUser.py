from django.test import Client, TestCase
from rrapp.models import Grupa, Korisnik # type: ignore
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User, Permission
from django.core import serializers
import json

class UserTestCase(TestCase):

    #Setup stvara Direktor korisnika, ulogira ga i sprema njegov Client() objekt
    def setUp(self):
        newuser = User.objects.create_user(username='Direktor', email='Direktor@mail.com', password='Direktor')
        newuser.save()

        #Get permission od Grupa tablice
        content_type_group = ContentType.objects.get_for_model(Grupa) 
        groupPermission = Permission.objects.get(
            codename='can_modify_groups',
            name='Can modify groups',
            content_type=content_type_group
        )

        #Get permission od Korisnik tablice
        content_type_user = ContentType.objects.get_for_model(Korisnik)
        userPermission = Permission.objects.get(
            codename='can_modify_users',
            name='Can modify users',
            content_type=content_type_user
        )

        newuser.user_permissions.add(userPermission)
        newuser.user_permissions.add(groupPermission)

        self.direktor = Korisnik(ime='Direktor', prezime='Direktor', username='Direktor', uloga='D', kontakt='Direktor@mail.com', korisnik=newuser)
        self.direktor.save()

        self.c = Client()
        response = self.c.post('/login', {'username':'Direktor', 'password':'Direktor'})

    #Direktor stvara Radnik i Voditelj korisnike koristeci /createuser endpoint.
    #Nakon uspjesnog stvaranja korisnika server vraca json podataka vezanih uz stvorenog korisnika.
    #Test ih usporeduje sa podatcima iz baze relevantne za tog korisnika i vraca error ako se ne poklapaju
    def test_create_users(self):
        radnik = self.c.post('/createuser', {'username':'Radnik', 'email':'Radnik@mail.com', 'password':'Radnik', 'uloga':'R', 'ime':'Radnik', 'prezime':'Radnik'})
        radnik_db = Korisnik.objects.get(username='Radnik')
        radnik_data = serializers.serialize('json', [radnik_db])

        self.assertEqual(radnik.json(), json.loads(radnik_data))


        voditelj = self.c.post('/createuser', {'username':'Voditelj', 'email':'Voditelj@mail.com', 'password':'Voditelj', 'uloga':'V', 'ime':'Voditelj', 'prezime':'Voditelj'})
        voditelj_db = Korisnik.objects.get(username='Voditelj')
        voditelj_data = serializers.serialize('json', [voditelj_db])

        self.assertEqual(voditelj.json(), json.loads(voditelj_data))