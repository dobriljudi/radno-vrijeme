from django.test import Client, TestCase
from django.test.utils import require_jinja2
from rrapp.models import Grupa, Korisnik # type: ignore
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User, Permission
from django.core import serializers
import json

class PermissionTestCase(TestCase):

    #Setup stvara Direktor, Radnik i Voditelj korisnike te ih ulogira i sprema njihov Client() objekt
    #S obzirom da je baza prazna u pocetku trebam rucno raditi Direktor korisnika da onda preko njega mogu ostale stvoriti
    def setUp(self):
        newuser = User.objects.create_user(username='Direktor', email='Direktor@mail.com', password='Direktor')
        newuser.save()

        #Get permission od Grupa tablice
        content_type_group = ContentType.objects.get_for_model(Grupa) 
        groupPermission = Permission.objects.get(
            codename='can_modify_groups',
            name='Can modify groups',
            content_type=content_type_group
        )

        #Get permission od Korisnik tablice
        content_type_user = ContentType.objects.get_for_model(Korisnik)
        userPermission = Permission.objects.get(
            codename='can_modify_users',
            name='Can modify users',
            content_type=content_type_user
        )

        newuser.user_permissions.add(userPermission)
        newuser.user_permissions.add(groupPermission)

        self.direktor = Korisnik(ime='Direktor', prezime='Direktor', username='Direktor', uloga='D', kontakt='Direktor@mail.com', korisnik=newuser)
        self.direktor.save()

        self.direktor = Client()
        self.direktor.post('/login', {'username':'Direktor', 'password':'Direktor'})
        self.direktor.post('/createuser', {'username':'Radnik', 'email':'Radnik@mail.com', 'password':'Radnik', 'uloga':'R', 'ime':'Radnik', 'prezime':'Radnik'})
        self.direktor.post('/createuser', {'username':'Voditelj', 'email':'Voditelj@mail.com', 'password':'Voditelj', 'uloga':'V', 'ime':'Voditelj', 'prezime':'Voditelj'})

        self.radnik = Client()
        self.radnik.post('/login', {'username':'Radnik', 'password':'Radnik'})
        self.voditelj = Client()
        self.voditelj.post('/login', {'username':'Voditelj', 'password':'Voditelj'})


    #Pokusa stvoriti grupu koristeci /creategroup endpoint iz perspektive Direktora, Voditelja i Radnika.
    #Ako korisnik ima pristup funkcionalnosti server vraca status code 200 a ako nema vraca 302
    def test_create_group_permission(self):
        voditelj = Korisnik.objects.get(username='Voditelj')
        response = self.direktor.post('/creategroup', {'voditelj':voditelj.id, 'ime_grupe':'Grupa1'})

        self.assertEqual(response.status_code, 200)

        response = self.voditelj.post('/creategroup', {'voditelj':voditelj.id, 'ime_grupe':'Grupa2'})
        self.assertEqual(response.status_code, 302)

        response = self.radnik.post('/creategroup', {'voditelj':voditelj.id, 'ime_grupe':'Grupa3'})
        self.assertEqual(response.status_code, 302)

    #Pokusa dobiti podatke o grupi koristeci /getgroupdata endpoint iz perspektive Direktora, Voditelja i Radnika.
    #Ako korisnik ima pristup funkcionalnosti server vraca status code 200 a ako nema vraca 302
    def test_get_group_data_permission(self):
        voditelj = Korisnik.objects.get(username='Voditelj')
        #Treba stvoriti grupu jer se baza brise izmedu testova
        response = self.direktor.post('/creategroup', {'voditelj':voditelj.id, 'ime_grupe':'Grupa1'})
        grupa = Grupa.objects.get(voditelj=voditelj)

        response = self.direktor.get('/getgroupdata', {'grupa':grupa.id})
        self.assertEqual(response.status_code, 200)

        response = self.voditelj.get('/getgroupdata', {'grupa':grupa.id})
        self.assertEqual(response.status_code, 302)

        response = self.radnik.post('/creategroup', {'voditelj':voditelj.id, 'ime_grupe':'Grupa3'})
        self.assertEqual(response.status_code, 302)


    #Pokusa stvoriti djelatnost koristeci /createaddgrouptask endpoint iz perspektive Direktora, Voditelja i Radnika.
    #Ako korisnik ima pristup funkcionalnosti server vraca status code 200 a ako nema vraca 302
    def test_create_djelatnost_permission(self):
        voditelj = Korisnik.objects.get(username='Voditelj')
        #Treba stvoriti grupu jer se baza brise izmedu testova
        response = self.direktor.post('/creategroup', {'voditelj':voditelj.id, 'ime_grupe':'Grupa1'})
        grupa = Grupa.objects.get(voditelj=voditelj)

        response = self.direktor.post('/createaddgrouptask', {'grupa':grupa.id, 'ime':'Djelatnost1', 'opis':'Djelatnost', 'satnica':'100'})
        self.assertEqual(response.status_code, 200)

        response = self.voditelj.post('/createaddgrouptask', {'grupa':grupa.id, 'ime':'Djelatnost2', 'opis':'Djelatnost', 'satnica':'100'})
        self.assertEqual(response.status_code, 302)

        response = self.radnik.post('/createaddgrouptask', {'grupa':grupa.id, 'ime':'Djelatnost3', 'opis':'Djelatnost', 'satnica':'100'})
        self.assertEqual(response.status_code, 302)