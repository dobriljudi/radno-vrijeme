from django.test import Client, TestCase
from rrapp.models import Grupa, Korisnik # type: ignore
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User, Permission
from django.core import serializers
import json
import sys

class GroupTestCase(TestCase):

    #Setup stvara Direktor korisnika, ulogira ga i sprema njegov Client() objekt
    #Takoder stvara dva Radnik korisnika i jednog Voditelj korisnika koje koristi u testovima
    def setUp(self):
        newuser = User.objects.create_user(username='Direktor', email='Direktor@mail.com', password='Direktor')
        newuser.save()

        #Get permission od Grupa tablice
        content_type_group = ContentType.objects.get_for_model(Grupa) 
        groupPermission = Permission.objects.get(
            codename='can_modify_groups',
            name='Can modify groups',
            content_type=content_type_group
        )

        #Get permission od Korisnik tablice
        content_type_user = ContentType.objects.get_for_model(Korisnik)
        userPermission = Permission.objects.get(
            codename='can_modify_users',
            name='Can modify users',
            content_type=content_type_user
        )

        newuser.user_permissions.add(userPermission)
        newuser.user_permissions.add(groupPermission)

        self.direktor = Korisnik(ime='Direktor', prezime='Direktor', username='Direktor', uloga='D', kontakt='Direktor@mail.com', korisnik=newuser)
        self.direktor.save()

        self.c = Client()
        response = self.c.post('/login', {'username':'Direktor', 'password':'Direktor'})
        t = self.c.post('/createuser', {'username':'Radnik1', 'email':'Radnik1@mail.com', 'password':'Radnik1', 'uloga':'R', 'ime':'Radnik1', 'prezime':'Radnik1'})
        print(t)
        self.c.post('/createuser', {'username':'Radnik2', 'email':'Radnik2@mail.com', 'password':'Radnik2', 'uloga':'R', 'ime':'Radnik2', 'prezime':'Radnik2'})
        self.c.post('/createuser', {'username':'Voditelj', 'email':'Voditelj@mail.com', 'password':'Voditelj', 'uloga':'V', 'ime':'Voditelj', 'prezime':'Voditelj'})


    #Direktor stvara grupu koristeci /creategoup endpoint.
    #Nakon uspjesnog stvaranja grupe server vraca json podatake vezane uz stvorenu grupu.
    #Test ih usporeduje s podatcima iz baze te vraca error ako se ne poklapaju
    def test_create_group(self):
        voditelj = Korisnik.objects.get(username='Voditelj')

        grupa = self.c.post('/creategroup', {'voditelj':voditelj.id, 'ime_grupe':'Grupa'})
        grupa_db = Grupa.objects.get(voditelj=voditelj)
        grupa_data = serializers.serialize('json', [grupa_db])

        self.assertEqual(grupa.json(), json.loads(grupa_data))
        pass

    #Direktor dodaje oba Radnik korisnika u stvorenu grupu koristeci /addusertogroup endpoint.
    #Nakon uspjesnog dodavanja korisnika server vraca json podatke grupe te i usporeduje s podatcima iz baze.
    #Ako se ne poklapaju vraca error
    def test_add_user_to_group(self):
        voditelj = Korisnik.objects.get(username='Voditelj')
        radnik1 = Korisnik.objects.get(username='Radnik1')
        radnik2 = Korisnik.objects.get(username='Radnik2')

        grupa = self.c.post('/creategroup', {'voditelj':voditelj.id, 'ime_grupe':'Grupa'})
        grupa_db = Grupa.objects.get(voditelj=voditelj)

        grupa = self.c.post('/addusertogroup', {'djelatnik':radnik1.id, 'ime_grupe':grupa_db.id})
        grupa = self.c.post('/addusertogroup', {'djelatnik':radnik2.id, 'ime_grupe':grupa_db.id})

        grupa_db = Grupa.objects.get(voditelj=voditelj)
        grupa_data = serializers.serialize('json', [grupa_db])

        self.assertEqual(grupa.json(), json.loads(grupa_data))

        grupa = self.c.post('/removeuserfromgroup', {'clan':radnik1.id, 'ime_grupe':grupa_db.id})
        grupa = self.c.post('/removeuserfromgroup', {'clan':radnik2.id, 'ime_grupe':grupa_db.id})

        grupa_db = Grupa.objects.get(voditelj=voditelj)
        grupa_data = serializers.serialize('json', [grupa_db])

        self.assertEqual(grupa.json(), json.loads(grupa_data))



