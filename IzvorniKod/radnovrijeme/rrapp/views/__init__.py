from .home import HomeController
from .direktor import DirektorController
from .group import GroupController
from .voditelj import VoditeljController
from .radnik import RadnikController