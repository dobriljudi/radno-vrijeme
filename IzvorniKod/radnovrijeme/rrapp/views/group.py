from django.forms.models import model_to_dict
from django.http.response import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.views import View
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.core import serializers
import json

from ..models import Grupa, Korisnik, Djelatnost, Zadatak

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('rrapp.can_modify_groups'), name='dispatch')
class GroupController(View):

    action = ''

    def get(self, request):
        if self.action == 'getgrouptasks':
            return self.getgrouptasks(request)
        elif self.action == 'getgroupdata':
            return self.getgroupdata(request)
        elif self.action == 'getallgrouptasks':
            return self.getallgrouptasks(request)
        elif self.action == 'getgrouptaskdata':
            return self.getgrouptaskdata(request)
        elif self.action == 'getalltasksforgrouptasks':
            return self.getalltasksforgrouptasks(request)
        pass

    def post(self, request):

        if self.action == 'creategroup':
            return self.creategroup(request)

        if self.action == 'deletegroup':
            return self.deletegroup(request)

        elif self.action == 'addusertogroup':
            return self.addusertogroup(request)

        elif self.action == 'removeuserfromgroup':
            return self.removeuserfromgroup(request)

        elif self.action == 'deletegrouptask':
            return self.deletegrouptask(request)

        elif self.action == 'createaddgrouptask':
            return self.createaddgrouptask(request)

        elif self.action == 'modifygrouptask':
            return self.modifygrouptask(request)

        pass

    def addusertogroup(self, request):

        try:

            #ID grupe i user ID
            djelatnikID = request.POST['djelatnik']
            grupaID = request.POST['ime_grupe']

            #objekti djelatnika i grupe. Sa stranice dolazi User ID
            djelatnik = Korisnik.objects.get(pk=djelatnikID)
            grupa = Grupa.objects.get(pk=grupaID)

            #Dodaje clana u grupu. .save() je implicitan
            grupa.clanovi.add(djelatnik)


            data = serializers.serialize('json', [grupa])
        except:
            return HttpResponseBadRequest("Error adding user to group")

        return HttpResponse(data, content_type='application/json')

    def removeuserfromgroup(self, request):

        try:
            clan_id = request.POST['clan']
            #TODO iz nekog razloga test stranica vraca POST varijablu sa 'ime_grupe' umjesto 'grupa'
            grupa_id = request.POST['ime_grupe']

            grupa = Grupa.objects.get(pk=grupa_id)
            clan = Korisnik.objects.get(pk=clan_id)
            grupa.clanovi.remove(clan)
        except Exception as e:
            return HttpResponseBadRequest("Error removing user from group")

        data = serializers.serialize('json', [grupa])

        return HttpResponse(data, content_type='application/json')

    def creategroup(self, request):
        #TODO FORM ZA CLEAN PODATKE

        try:
            #_id u korisnik_id je Django sufiks za pristup int vrijednosti id a ne objektu User
            voditelj = Korisnik.objects.get(pk=request.POST['voditelj'])

            #stvaranje objekta modela po imenu nove grupe i voditelju
            group = Grupa(ime_grupe=request.POST['ime_grupe'], voditelj=voditelj)
            group.save()

            groupData = serializers.serialize('json', [group])
        except:
            return HttpResponseBadRequest("Error creating group")

        return HttpResponse(groupData, content_type='application/json') 

    def deletegroup(self, request):

        #TODO FORM ZA CLEAN PODATKE

        try:
            grupaID = request.POST['ime_grupe']
            grupa = Grupa.objects.get(pk=grupaID)
            grupa.delete()

        except:
            return HttpResponseBadRequest("Group does not exist")

        return HttpResponse("Deleted group")

    def deletegrouptask(self, request):

        try:

            djelatnost = Djelatnost.objects.get(pk=request.POST['id'])
            djelatnost.delete()
        except:
            return HttpResponseBadRequest("Error deleting group")

        return HttpResponse()

    def createaddgrouptask(self, request):
        try:
            grupa = Grupa.objects.get(pk=request.POST['grupa'])
            djelatnost = Djelatnost(ime_djelatnosti=request.POST['ime'],
                                    opis_djelatnosti=request.POST['opis'],
                                    satnica_kn=request.POST['satnica'],
                                    grupa=grupa)
            djelatnost.save()

            data = serializers.serialize('json', [djelatnost])
        except:
            return HttpResponseBadRequest("Error creating/adding task")

        return HttpResponse(data, content_type='application/json')

    def modifygrouptask(self, request):
        try:
            #Modificira podatke djelatnosti za dan ID TODO provjera sta se tocno treba promijeniti a ne refresh svega
            djelatnost = Djelatnost.objects.get(pk=request.POST['id'])
            grupa = Grupa.objects.get(pk=request.POST['grupa'])

            djelatnost.ime_djelatnosti = request.POST['ime']
            djelatnost.opis_djelatnosti=request.POST['opis']
            djelatnost.satnica_kn=request.POST['satnica']
            djelatnost.grupa = grupa

            djelatnost.save()

            data = serializers.serialize('json', [djelatnost])
        except:
            return HttpResponseBadRequest("Error modifying group task")

        return HttpResponse(data, content_type='application/json')

    def getgroupdata(self, request):
        try:

            group_id = request.GET['grupa']
            group = Grupa.objects.get(pk=group_id)

            clanovi = group.clanovi.all()

            #serializira u json 
            group_data = serializers.serialize('json', [group])
            clanovi_data = serializers.serialize('json', clanovi)

            #vraca ga u dict 
            group_data = json.loads(group_data)
            clanovi_data = json.loads(clanovi_data)

            #dodaje dict clanova u dict grupe
            group_data[0]['fields']['clanovi'] = clanovi_data

            data = json.dumps(group_data[0])
        except:
            return HttpResponseBadRequest("Error getting group data")

        return HttpResponse(data, content_type='application/json')

    def getgrouptaskdata(self, request):
        try:
            djelatnost_id = request.GET['djelatnost'] 
            djelatnost = Djelatnost.objects.get(pk=djelatnost_id)

            data = serializers.serialize('json', [djelatnost])
        except:
            return HttpResponseBadRequest("Error getting group task data")

        return HttpResponse(data, content_type='application/json')

    def getgrouptasks(self, request):
        try:
            group_id = request.GET['grupa']
            grupa = Grupa.objects.get(pk=group_id)
            
            djelatnosti = Djelatnost.objects.filter(grupa=grupa)

            data = serializers.serialize('json', djelatnosti)
        except:
            return HttpResponseBadRequest("Error getting group tasks")

        return HttpResponse(data, content_type='application/json')
    
    def getallgrouptasks(self, request):
        djelatnosti = Djelatnost.objects.all()

        data = serializers.serialize('json', djelatnosti)

        return HttpResponse(data, content_type='application/json')

