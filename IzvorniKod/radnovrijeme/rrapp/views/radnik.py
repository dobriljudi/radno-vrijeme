from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Group, Permission, User
from django.contrib.contenttypes.models import ContentType
from django.core import serializers
from django.http.response import HttpResponse, HttpResponseBadRequest
from django.utils.decorators import method_decorator
from django.shortcuts import redirect, render
from django.views import View
from django.db.models import Sum
from datetime import date
from math import floor
import json

from ..models import Evidencija, Grupa, Korisnik, Zadatak, Ima_zadatak, Djelatnost

@method_decorator(login_required, name='dispatch')
class RadnikController(View):
    action = ''

    def get(self, request):
        if self.action == 'getcurrentusertasks':
            return self.getcurrentusertasks(request)
        elif self.action == 'getcurrentusergroups':
            return self.getcurrentusergroups(request)
        elif self.action == 'gettask':
            return self.gettask(request)
        elif self.action == 'getpercentage':
            return self.getpercentage(request)
        pass

    def post(self, request):
        if self.action == 'createrecord':
            return self.createrecord(request)
        pass

    def createrecord(self, request):
        try:
            korisnik = Korisnik.objects.get(korisnik=request.user)

            if korisnik.uloga != 'R':
                return HttpResponseBadRequest("User not radnik")

            zadatak = Zadatak.objects.get(id=request.POST['zadatak'])
            satnica = request.POST['satnica']
            if satnica == 0:
                return HttpResponseBadRequest("Evidencija od 0 sati nije evidencija!")
            opis = request.POST['opis']
            e = Evidencija(korisnik=korisnik, zadatak=zadatak, satnica=satnica, opis=opis)
            e.save()

            data = serializers.serialize('json', [e])

            #Broj evidentiranih sati za zadatak
            evidentirano = Evidencija.objects.filter(zadatak=zadatak).aggregate(Sum('satnica'))
            evidentirano = evidentirano['satnica__sum']

            predvideno = zadatak.predvideno_trajanje
            #Ako se evidentiralo jednako ili vise broja sati od predvidenih zadatak je zavrsen i micemo ga svima koji ga imaju
            if evidentirano >= predvideno:
                zadatak.stanje = 'Z'
                zadatak.save()

                #Svi korisnici koji imaju ovaj zadatak
                korisnici = Korisnik.objects.filter(zadatak=zadatak)

                #Brisemo vezu zadatka
                for k in korisnici:
                    k.zadatci.remove(zadatak)
                
        except Exception as e:
            return HttpResponseBadRequest("Error" + str(e))

        return HttpResponse(data, content_type='application/json')

    def getcurrentusertasks(self, request):
        try:
            korisnik = Korisnik.objects.get(korisnik=request.user)
            zadatci = korisnik.zadatci.all()

            data = serializers.serialize('json', zadatci)
        except Exception as e:
            return HttpResponseBadRequest("Error" + str(e))

        return HttpResponse(data, content_type='application/json')

    def getcurrentusergroups(self, request):
        try:
            korisnik = Korisnik.objects.get(korisnik=request.user)
            grupe = Grupa.objects.filter(clanovi=korisnik)

            data = serializers.serialize('json', grupe)
        except Exception as e:
            return HttpResponseBadRequest("Error" + str(e))

        return HttpResponse(data, content_type='application/json')

    def gettask(self, request):
        try:
            zadatak = Zadatak.objects.get(pk=request.GET['id'])

            data = serializers.serialize('json', [zadatak])
        except Exception as e:
            return HttpResponseBadRequest("Error" + str(e))

        return HttpResponse(data, content_type='application/json')

    def getpercentage(self, request):
        try:
            korisnik = Korisnik.objects.get(korisnik=request.user)

            if korisnik.uloga != 'R':
                return HttpResponseBadRequest("Error user not radnik")

            satnica = Ima_zadatak.objects.filter(korisnik=korisnik).aggregate(Sum('trajanje_zadatka'))
            ukupno = satnica['trajanje_zadatka__sum']

            evidencije = Evidencija.objects.filter(timestamp__date=date.today(), korisnik=korisnik)
            odradjeno = evidencije.aggregate(Sum('satnica'))['satnica__sum']

            postotak = 0
            if(odradjeno is not None):
                postotak = odradjeno / ukupno
                postotak = floor(postotak * 100)

            data = json.dumps(postotak)          

        except Exception as e:
            print(e)
            return HttpResponseBadRequest("Error" + str(e))
        return HttpResponse(data, content_type='application/json')
