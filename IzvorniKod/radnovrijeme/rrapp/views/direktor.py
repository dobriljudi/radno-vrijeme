import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Permission, User
from django.contrib.contenttypes.models import ContentType
from django.core import serializers
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.forms.models import model_to_dict
from django.http import JsonResponse, request
from django.http.response import HttpResponse, HttpResponseBadRequest
from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.views import View

from ..forms import (AddActivityForm, CreateUserForm, GroupNameForm,
                     ListUserForm, ListGroupsForm)
from ..models import Grupa, Korisnik, Zadatak, Ima_zadatak


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('rrapp.can_modify_users'), name='dispatch')
@method_decorator(permission_required('rrapp.can_modify_groups'), name='dispatch')
class DirektorController(View):

    action = ''

    def get(self, request):
        if self.action == 'createuserpage':
            return self.createuserpage(request)
        elif self.action == 'managegrouppage':
            return self.managegrouppage(request)
        elif self.action == 'managetaskpage':
            return self.managetaskpage(request)

    def post(self, request):

        if self.action == 'createuser':
            return self.createuser(request)

        elif self.action == 'deleteuser':
            return self.deleteuser(request)

        elif self.action == 'modifyuser':
            return self.modifyuser(request)

    def createuser(self, request):
        
        
        form = CreateUserForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data

            #Stvaranje novog korisnika u Django user tablici 
            newuser = User.objects.create_user(username=data['username'],  email=data['email'], password=data['password'])
            newuser.save()
            
            #Stvaranje novog korinika u Korisnik tablici i povezivanje s novim korinikom u Django User tablici (OneToOne)
            korisnik = Korisnik(ime=data['ime'], prezime=data['prezime'], username=data['username'], uloga=data['uloga'], kontakt=data['email'], korisnik=newuser)
            korisnik.save()

            #Get permission od Grupa tablice
            content_type_group = ContentType.objects.get_for_model(Grupa) 
            groupPermission = Permission.objects.get(
                codename='can_modify_groups',
                name='Can modify groups',
                content_type=content_type_group
            )

            #Get permission od Korisnik tablice
            content_type_user = ContentType.objects.get_for_model(Korisnik)
            userPermission = Permission.objects.get(
                codename='can_modify_users',
                name='Can modify users',
                content_type=content_type_user
            )

            content_type_task = ContentType.objects.get_for_model(Zadatak)
            taskPermission = Permission.objects.get(
                codename='can_modify_tasks',
                name='Can modify tasks',
                content_type=content_type_task
            )

            #TODO odluci jeli bolje/lakse dodavati permissione userima ili dodavati usere u grupe
            if data['uloga'] == 'D' or data['uloga'] == 'Direktor':
                #Davanje dozvole korisniku s ulogom direktor
                newuser.user_permissions.add(userPermission)
                newuser.user_permissions.add(groupPermission)
            elif data['uloga'] == 'V' or data['uloga'] == 'Voditelj':
                #Dodavanje dozvole mijenjanja Zadatak tablice voditelju
                newuser.user_permissions.add(taskPermission)
                pass

            data = serializers.serialize('json', [korisnik])
            return HttpResponse(data, content_type='application/json')

        else:
            return HttpResponseBadRequest(json.dumps(form.errors), content_type = 'application/json')
        
        
    def modifyuser(self, request):
        try:
            korisnik = Korisnik.objects.get(pk=request.POST['id'])
            user = User.objects.get(pk=korisnik.korisnik_id)

            korisnik.ime = request.POST['ime']
            korisnik.prezime = request.POST['prezime']
            korisnik.uloga = request.POST['uloga']
            korisnik.kontakt = request.POST['kontakt']
            korisnik.slika_path = request.POST['slika_path']
            korisnik.username = request.POST['username']

            user.username = request.POST['username']
            user.set_password(request.POST['password'])

            korisnik.save()
            user.save()

            data = serializers.serialize('json', [korisnik])
        except:
            return HttpResponseBadRequest("Error modifying user")

        return HttpResponse(data, content_type='application/json')
        pass


    def deleteuser(self, request):

        #Brisanje korisnika odabranog u drop down tablici u html formu
        #Ne trebamo dodatno brisati entry u Korisnik tablici jer je on_delete=CASCADE tako da se automatski brise
        try:
            korisnik = Korisnik.objects.get(pk=request.POST['user'])
            user = korisnik.korisnik

            user.delete()
            korisnik.delete()
        except User.DoesNotExist:
            return HttpResponseBadRequest("User does not exist")

        return HttpResponse()

    def createuserpage(self, request):

        #Uzima sve korisnike iz Djangove User tablice i salje ih html-u da se mogu prikazati u dropdown      
        #form = CreateUserForm()
        users = Korisnik.objects.all()

        data = serializers.serialize('json', users)

        return render(request, 'direktor/direktor_radnik.html', {'korisnici_data':data})
        

    def managegrouppage(self, request):

        #Uzima User objekte svih korisnika koji u Korisnik tablici imaju ulogu voditelja/radnika
        #valja napomenuti da se salje Korisnik objekt ID a ne User ID
        voditelji = [k for k in Korisnik.objects.filter(uloga='V')]
        djelatnici = [k for k in Korisnik.objects.filter(uloga='R')]
        grupe = [g for g in Grupa.objects.all()]
        
        djelatnici_data = serializers.serialize('json', djelatnici)
        grupe_data = serializers.serialize('json', grupe) 
        voditelji_data = serializers.serialize('json', voditelji)
        
        # createGroupForm = GroupNameForm()
        # listUserForm = ListUserForm(usernames = voditelji, lbl = "Voditelj")
        # listUserFormAll = ListUserForm(usernames = djelatnici, lbl = "Djelatnik")  
        # listGroupsForm = ListGroupsForm(id = 'id_ime_grupe')
        # listGroupsFormRemove = ListGroupsForm(id = 'id_grupe') 
        

        context = {
        #     'group_name' : createGroupForm,
        #     'list_leaders' : listUserForm,
        #     'list_groups' : listGroupsForm,
        #     'list_groups_remove' : listGroupsFormRemove,
        #     'list_all' : listUserFormAll,
            'djelatnici_data':djelatnici_data, 
            'grupe_data':grupe_data, 
            'voditelji_data':voditelji_data
        }

        return render(request, 'direktor/direktor_grupa.html', context = context)

    def managetaskpage(self, request):

        grupe = [g for g in Grupa.objects.all()]
        grupe_data = serializers.serialize('json', grupe) 
        # dataDict = [{'id':g.id, 'grupa':g.ime_grupe} for g in grupe]

        # form = AddActivityForm()

        return render(request, 'direktor/direktor_djelatnost.html', {'grupe_data': grupe_data})

    