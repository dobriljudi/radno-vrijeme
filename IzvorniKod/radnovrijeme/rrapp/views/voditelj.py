import json
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Permission, User
from django.contrib.contenttypes.models import ContentType
from django.core import serializers
from django.http.response import HttpResponse, HttpResponseBadRequest
from django.utils.decorators import method_decorator
from django.shortcuts import redirect, render
from django.views import View

from ..models import Evidencija, Grupa, Korisnik, Zadatak, Ima_zadatak, Djelatnost

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('rrapp.can_modify_tasks'), name='dispatch')
class VoditeljController(View):
    action = ''

    def get(self, request):
        if self.action == 'getusertasks':
            return self.getusertasks(request)
        elif self.action == 'getleadergroup':
            return self.getleadergroup(request)
        elif self.action == 'gettaskusers':
            return self.gettaskusers(request)
        elif self.action == 'gettaskpage':
            return self.gettaskpage(request)
        elif self.action == 'getalltasksforgrouptasks':
            return self.getalltasksforgrouptasks(request)
        elif self.action == 'getleaderstatisticspage':
            return self.getleaderstatisticspage(request)
        
        pass

    def post(self, request):
        if self.action == 'createtask':
            return self.createtask(request)
        elif self.action == 'addtasktouser':
            return self.addtasktouser(request)
        elif self.action == 'removetaskfromuser':
            return self.removetaskfromuser(request)
        elif self.action == 'deletetask':
            return self.deletetask(request)
        elif self.action == 'modifytask':
            return self.modifytask(request)
        pass

    def createtask(self, request):
        try:
            djelatnost = Djelatnost.objects.get(pk=request.POST['djelatnost'])
            zadatak = Zadatak(ime=request.POST['ime'], 
                            opis=request.POST['opis'],
                            satnica=request.POST['satnica'],
                            predvideno_trajanje=request.POST['trajanje'],
                            djelatnost=djelatnost,
                            stanje='A')
            zadatak.save()

            data = serializers.serialize('json', [zadatak])
        except Exception as e:
            return HttpResponseBadRequest("Error creating task" + str(e))

        return HttpResponse(data, content_type='application/json')

    def deletetask(self, request):
        try:
            zadatak = Zadatak.objects.get(pk=request.POST['zadatak'])
            zadatak.delete()

        except Exception as e:
            return HttpResponseBadRequest("Error deleting task" + str(e))
        
        return HttpResponse("Success")

    def modifytask(self, request):
        try:
            djelatnost = Djelatnost.objects.get(pk=request.POST['djelatnost'])
            zadatak = Zadatak.objects.get(pk=request.POST['id'])

            zadatak.ime = request.POST['ime']
            zadatak.opis = request.POST['opis']
            zadatak.stanica = request.POST['satnica']
            zadatak.predvideno_trajanje = request.POST['trajanje']
            zadatak.stanje = request.POST.get('stanje', 'A')
            djelatnost = djelatnost

            zadatak.save()

            data = serializers.serialize('json', [zadatak])

        except Exception as e:
            return HttpResponseBadRequest("Error " + str(e))

        return HttpResponse(data, content_type='application/json')

    def addtasktouser(self, request):
        try:
            korisnik = Korisnik.objects.get(pk=request.POST['korisnik'])
            zadatak = Zadatak.objects.get(pk=request.POST['zadatak'])
            trajanje = request.POST['trajanje']

            korisnik.zadatci.add(zadatak, through_defaults={'trajanje_zadatka':trajanje})

        except Exception as e:
            return HttpResponseBadRequest("Error adding task to user" + str(e))

        return HttpResponse("Success")

    def removetaskfromuser(self, request):
        try:
            korisnik = Korisnik.objects.get(pk=request.POST['korisnik'])

            korisnik.zadatci.remove(request.POST['zadatak'])
            #potencialno ne treba sve zadatke vracati
            data = serializers.serialize('json', korisnik.zadatci.all())

        except Exception as e:
            return HttpResponseBadRequest("Error " + str(e))

        return HttpResponse(data, content_type='application/json')

    def getusertasks(self, request):
        try:
            korisnik = Korisnik.objects.get(pk=request.GET['korisnik'])
            zadatci = korisnik.zadatci.all()

            data = serializers.serialize('json', zadatci)

        except Exception as e:
            return HttpResponseBadRequest("Error getting tasks" + str(e))

        return HttpResponse(data, content_type='application/json')

    def getleadergroup(self, request):
        try:
            korisnik = Korisnik.objects.get(pk=request.GET['korisnik'])

            if korisnik.uloga == 'V' or korisnik.uloga == 'Voditelj':

                grupa = Grupa.objects.get(voditelj=korisnik)

                data = serializers.serialize('json', [grupa])
            else:
                return HttpResponseBadRequest("Korisnik nije voditelj")

        except Exception as e:
            return HttpResponseBadRequest("Error " + str(e))

        return HttpResponse(data, content_type='application/json')

    def gettaskusers(self, request):
        try:
            zadatak = Zadatak.objects.get(pk=request.GET['zadatak'])
            korisnici = Korisnik.objects.filter(zadatci=zadatak)

            data = serializers.serialize('json', korisnici)

        except Exception as e:
            return HttpResponseBadRequest("Error " + str(e))

        return HttpResponse(data, content_type='application/json')

    def gettaskpage(self, request):
        try:
            korisnik = Korisnik.objects.get(korisnik=request.user)
            if korisnik.uloga == 'V' or korisnik.uloga == 'Voditelj':

                try:
                    grupa = Grupa.objects.get(voditelj=korisnik)
                    clanovi = grupa.clanovi.all()
                    djelatnosti = Djelatnost.objects.filter(grupa=grupa)

                    clanovi_data = serializers.serialize('json', clanovi)
                    djelatnosti_data = serializers.serialize('json', djelatnosti)

                    context = {
                        'djelatnosti_data' : djelatnosti_data,
                        'clanovi_data' : clanovi_data
                    }
                except Grupa.DoesNotExist:
                    context ={}

            else:
                return HttpResponseBadRequest("User not Voditelj")

        except Exception as e:
            return HttpResponseBadRequest("Error " + str(e))

        return render(request, 'voditelj/voditelj_zadatak.html', context=context)

    def getalltasksforgrouptasks(self, request):
        try:
            djelatnost = Djelatnost.objects.get(pk=request.GET['djelatnost'])
            zadatci = Zadatak.objects.filter(djelatnost=djelatnost)

            data = serializers.serialize('json', zadatci)

        except Exception as e:
            return HttpResponseBadRequest("Error" + str(e))

        return HttpResponse(data, content_type='application/json')

    def getleaderstatisticspage(self, request):
        try:
            korisnik = Korisnik.objects.get(korisnik=request.user)
            if korisnik.uloga == 'V':
                grupa = Grupa.objects.get(voditelj=korisnik)
                clanovi = grupa.clanovi.all()
                evidencije = Evidencija.objects.filter(korisnik__in=clanovi)

                context = {
                    'grupa_data' : serializers.serialize('json', [grupa]),
                    'clanovi_data' : serializers.serialize('json', clanovi),
                    'evidencije_data' : serializers.serialize('json', evidencije)
                }

                pass
            else:
                return HttpResponseBadRequest("User not Voditelj")
            pass
        except Exception as e:
            return HttpResponseBadRequest("Error" + str(e))

        return render(request, 'voditelj/voditelj_statistika.html', context=context)




        

        