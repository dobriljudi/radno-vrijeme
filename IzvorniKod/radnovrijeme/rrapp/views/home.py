from django.core import serializers
from django.core.serializers import serialize
from django.http import request
from django.http.response import HttpResponse, HttpResponseBadRequest
from django.shortcuts import redirect, render
from django.views import View
from django.contrib.auth import authenticate, login, logout
from ..models import Korisnik, Djelatnost, Grupa
import json

#Kontroler za login/logout i genericne funkcionalnosti
class HomeController(View):
    action = ''

    #Vraca login stranicu
    def get(self, request):

        if self.action == 'getcurrentuser':
            return self.getcurrentuser(request)
        
        if self.action == 'error':
            return self.nopermission(request)

        #Ako je user vec ulogiran redirect na njegov homepage
        if request.user.is_authenticated:
            if self.action == 'profile':
                return self.profile(request)
                # return render(request, 'profil/profil_korisnika.html')
            elif self.action == 'getuser':
                return self.getuser(request)

            return self.home(request)

        return self.welcomepage(request)

    def post(self, request):

        #Ako je POST poslan na rrapp/login endpoint
        if self.action == 'login':

            #Ako je User vec ulogiran redirektaj na homepage
            if request.user.is_authenticated:
                return self.home(request)

            #Ako su username i password ispunjeni
            if 'username' in request.POST and 'password' in request.POST:
                username = request.POST['username']
                password = request.POST['password']
                user = authenticate(request, username=username, password=password)

                #Ako su username i password tocni
                if user is not None:
                    login(request, user)
                    return self.home(request)

                #Ako nisu redirectaj na login s errorom (treba dodati error)
                else:
                   return HttpResponseBadRequest("Netočan username ili password!")
            else:
                #Ako je login kliknut s praznim poljima
                return self.welcomepage(request)

        #Ako je POST poslan na rrapp/logout endpoint
        elif self.action == 'logout':
            logout(request)
            return self.welcomepage(request)

        elif self.action == 'modifycurrentuser':
            return self.modifycurrentuser(request)

    def modifycurrentuser(self, request):

        try:
            korisnik = Korisnik.objects.get(korisnik=request.user)
            user = request.user

            korisnik.ime = request.POST['ime']
            korisnik.prezime = request.POST['prezime']
            korisnik.kontakt = request.POST['kontakt']
            korisnik.save()
            
            password = request.POST['password']
            if(password != ''):
                user.set_password(password)
                user.save()

            if(request.POST['slika_path'] != ''):
                warning = 'Na žalost nije još nije podržan upload slika!'
                return HttpResponse(json.dumps(warning), content_type='application/json')

        except:
            return HttpResponseBadRequest("Error modifying user")

        warning = ""
        return HttpResponse(json.dumps(warning), content_type='application/json')
        pass
    
    def home(self, request):

        #Korisnik objekt iz baze podataka
        korisnik = Korisnik.objects.get(korisnik=request.user.id)

        if korisnik.uloga == 'R':
            try:

                korisnik = Korisnik.objects.get(korisnik=request.user)
                grupe = Grupa.objects.filter(clanovi=korisnik)
                zadatci = korisnik.zadatci.all()
                djelatnosti = Djelatnost.objects.all()

                grupe_data = serializers.serialize('json', grupe)
                zadatci_data = serializers.serialize('json', zadatci)
                djelatnosti_data = serializers.serialize('json', djelatnosti)

                context = {
                    'grupe_data' : grupe_data,
                    'zadatci_data' : zadatci_data,
                    'djelatnosti_data' : djelatnosti_data
                }

            except Exception as e:
                return HttpResponseBadRequest("Error " + str(e))

            return render(request, 'radnik/radnik_pocetna.html', context=context)

        elif korisnik.uloga == 'V':
            try:

                grupa = Grupa.objects.get(voditelj=korisnik)
            except Grupa.DoesNotExist:
                return render(request, 'voditelj/voditelj_pocetna.html', context={'message': 'Niste dodijeljeni niti jednoj grupi!'})

            return render(request, 'voditelj/voditelj_pocetna.html')
        elif korisnik.uloga == 'D':
            return render(request, 'direktor/direktor_pocetna.html')

    def getuser(self, request):

        try:
            k = Korisnik.objects.get(pk=request.GET['id'])
            data = serializers.serialize('json', [k])
        except:
            return HttpResponseBadRequest("Error getting user")

        return HttpResponse(data, content_type='application/json')

    def getcurrentuser(self, request):
        if request.user.is_authenticated:
            korisnik = Korisnik.objects.get(korisnik=request.user)

            data = serializers.serialize('json', [korisnik])
        else:
            data = "[{}]"

        return HttpResponse(data, content_type='application/json')

    def profile(self, request):
        try:
            if 'id' in request.GET:
                korisnik = Korisnik.objects.get(pk=request.GET['id'])
            else:
                korisnik = Korisnik.objects.get(korisnik=request.user)

            data = serializers.serialize('json', [korisnik])
            timestamp = request.user.date_joined

        except:
            return HttpResponseBadRequest("Error getting user data")

        
        return render(request, 'profil/profil_korisnika.html', {'data':data, 'date_joined':timestamp})

    def welcomepage(self, request):
        djelatnosti = Djelatnost.objects.all()
        data = serializers.serialize('json', djelatnosti)
        
        return render(request, 'neregistrirani_korisnik_pocetna.html', {'djelatnosti_data':data})

    def nopermission(self, request):
        return render(request, 'error/error.html')


