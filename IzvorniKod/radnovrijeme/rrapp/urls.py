from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.HomeController.as_view(action = '')),

    path('login', views.HomeController.as_view(action='login')),
    path('logout', views.HomeController.as_view(action='logout')),
    path('profile', views.HomeController.as_view(action='profile'), name='profile'),
     path('error', views.HomeController.as_view(action='error')),

    path('createuserpage', views.DirektorController.as_view(action='createuserpage'), name='createuserpage'),

    #ENDPOINT POST vraca novog korisnika sa svim potrebnim podatcima
    path('createuser', views.DirektorController.as_view(action='createuser')),
    #ENDPOINT POST vraca error ako user ne postoji
    path('deleteuser', views.DirektorController.as_view(action='deleteuser')),
    #ENDPOINT POST modificira podatke korisnika
    path('modifyuser', views.DirektorController.as_view(action='modifyuser')),

    #ENDPOINT POST modificira podatke ulogiranog korisnika
    path('modifycurrentuser', views.HomeController.as_view(action='modifycurrentuser')),

    #ENDPOINT GET vraca korisnik objekt za dan ID 
    path('getuser', views.HomeController.as_view(action='getuser')),
    #ENDPOINT GET vraca trenutno ulogiranog korisnika
    path('getcurrentuser', views.HomeController.as_view(action='getcurrentuser')),

    path('managegrouppage', views.DirektorController.as_view(action='managegrouppage'), name='managegrouppage'),

    #ENDPOINT POST vraca novo kreiranu grupu
    path('creategroup', views.GroupController.as_view(action='creategroup'), name='creategroup'),
    #ENDPOINT POST vraca error ako grupa ne postoji
    path('deletegroup', views.GroupController.as_view(action='deletegroup'), name='deletegroup'),

    #ENDPOINT POST vraca izmijenjenu grupu
    path('addusertogroup', views.GroupController.as_view(action='addusertogroup'), name='addusertogroup'),
    #ENDPOINT POST vraca error ako korisnik ili grupa ne postoje
    path('removeuserfromgroup', views.GroupController.as_view(action='removeuserfromgroup'), name='removeuserfromgroup'),

    path('managetaskpage', views.DirektorController.as_view(action='managetaskpage'), name='managetaskpage'),

    #ENDPOINT POST request koji modificira odabranu djelatnost
    path('modifygrouptask', views.GroupController.as_view(action='modifygrouptask'), name='modifygrouptask'),
    
    #ENDPOINT POST request stvara djelatnost i dodaje je odabranoj grupi
    path('createaddgrouptask', views.GroupController.as_view(action='createaddgrouptask'), name='createaddgrouptask'),
    #ENDPOINT POST brise djelatnost za dani ID
    path('deletegrouptask', views.GroupController.as_view(action='deletegrouptask'), name='deletegrouptask'),

    #ENDPOINT GET request za dani ID grupe vraca podatke o grupi te sve njene clanovi i njihove podatke
    path('getgroupdata', views.GroupController.as_view(action='getgroupdata'), name='getgroupadata'),
    #ENDPOINT GET request vraca podatke o djelatnosti za dani ID djelatnosti
    path('getgrouptaskdata', views.GroupController.as_view(action='getgrouptaskdata'), name='getgrouptaskdata'),
    #ENDPOINT GET request vraca listu djelatnosti koje pripadaju grupi
    path('getgrouptasks', views.GroupController.as_view(action='getgrouptasks'), name='getgrouptasks'),
    #ENDPOINT GET request vraca listu svih djelatnosti
    path('getallgrouptasks', views.GroupController.as_view(action='getallgrouptasks'), name='getallgrouptasks'),

    #ENDPOINT POST request koji stvara zadatak sa danim podatcima. Vraca json stvorenog zadatka
    path('createtask', views.VoditeljController.as_view(action='createtask')),
    #ENDPOINT POST request brise zadatak
    path('deletetask', views.VoditeljController.as_view(action='deletetask')),
    #ENDPOINT POST request ureduje zadatak
    path('modifytask', views.VoditeljController.as_view(action='modifytask')),
    #ENDPOINT POST request koji dodaje zadatak korisniku za dan user i task ID (Ima_Zadatak tablica)
    path('addtasktouser', views.VoditeljController.as_view(action='addtasktouser')),
    #ENDPOINT POST request koji brise zadatak dan korisniku (Ima_Zadatak tablica) vraca listu zadataka korisnika
    path('removetaskfromuser', views.VoditeljController.as_view(action='removetaskfromuser')),

    #ENDPOINT GET request vraca sve zadatke za danog korisnika
    path('getusertasks', views.VoditeljController.as_view(action='getusertasks')),
    #ENDPOINT GET request za Voditelja vraca grupu kojoj je dodijeljen
    path('getleadergroup', views.VoditeljController.as_view(action='getleadergroup')),
    #ENDPOINT GET request za dan Zadatak vraca korisnike koji ga imaju
    path('gettaskusers', views.VoditeljController.as_view(action='gettaskusers')),
    #EDPOINT GET request vraca sve zadatke za danu djelatnost
    path('getalltasksforgrouptasks', views.VoditeljController.as_view(action='getalltasksforgrouptasks')),
    
    path('gettaskpage', views.VoditeljController.as_view(action='gettaskpage')),
    path('getleaderstatisticspage', views.VoditeljController.as_view(action='getleaderstatisticspage')),

    #ENDPOINT GET request vraca sve zadatke za trenutno ulogiranog korisnika
    path('getcurrentusertasks', views.RadnikController.as_view(action='getcurrentusertasks')),
    #ENDPOINT GET request vraca grupe kojima trenutni korisnik pripada 
    path('getcurrentusergroups', views.RadnikController.as_view(action='getcurrentusergroups')),
    #ENDPOINT GET request za dan ID vraca podatke o zadatku 
    path('gettask', views.RadnikController.as_view(action='gettask')),

    #ENDPOINT POST stvara evidenciju rada trenutnog radnika za zadani zadatak
    path('createrecord', views.RadnikController.as_view(action='createrecord')),

    path('getpercentage', views.RadnikController.as_view(action='getpercentage'))









]
