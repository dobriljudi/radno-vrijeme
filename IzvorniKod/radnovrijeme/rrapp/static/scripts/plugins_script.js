$(document).ready(function() {
    today = new Date();
    dd = String(today.getDate()).padStart(2, '0');
    mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    yyyy = today.getFullYear();

    today = dd + '/' + mm + '/' + yyyy;
    $('#current-date').text(today)
})


//zasad moram ovako appendati id iz tablice u podatke
var transform = function(array) {
    return array.map(a => {
        let fields = a.fields
        fields.id = a.pk
        return fields
    })
}

var getActivity = async function(id) {
    var djelatnost = undefined
    await $.ajax({
        type: "GET",
        url: "/getgrouptaskdata",
        data: {
            'djelatnost': id,
            'csrfmiddlewaretoken': token
        },
        success: function(data) {
           djelatnost = transform(data)[0]
        },
        error: function(data){
            alert("Greska")
        }
    })
    return djelatnost
}

var getAllActivitiesFromGroup = async function(group_id) {
    var djelatnosti = undefined
    await $.ajax({
       type: "GET",
       url: "/getgrouptasks",
       data: {
           'grupa': group_id,
           'csrfmiddlewaretoken': token
       },
       success: function(data) {
          djelatnosti = transform(data)
       },
       error: function(data){
           alert("Greska")
       }
   })
   return djelatnosti
}

var getUser = async function(id) {
    var user = undefined
    await $.ajax({
        type: "GET",
        url: "/getuser",
        data: {
            'id': id,
            'csrfmiddlewaretoken': token
        },
        success: function(data) {
            user = transform(data)[0]
        },
        error: function(data){
            alert("Greska")
        }
    })
    return user
}

var getCurrentUser = async function() {
    var user = undefined
    await $.ajax({
        type: "GET",
        url: "/getcurrentuser",
        success: function(data) {
            user = transform(data)[0]
        },
        error: function(data){
            alert("Greska")
        }
    })
    return user
}

var goToOtherUserProfile = async function(id) {
    window.location = `/profile?id=${id}`
}

var getAllActivities = async function() {
    var djelatnosti = undefined
    await $.ajax({
        type: "GET",
        url: "/getallgrouptasks",
        data: {
            'csrfmiddlewaretoken': token
        },
        success: function(data) {
            console.log(data)
            djelatnosti = transform(data)
        },
        error: function(data){
            alert("Greska")
        }
    })

    return djelatnosti
}

var getAllTasksFromActivity = async function(djelatnost_id) {
    var zadatci = undefined
    await $.ajax({
       type: "GET",
       url: "/getalltasksforgrouptasks",
       data: {
           'djelatnost': djelatnost_id,
           'csrfmiddlewaretoken': token
       },
       success: function(data) {
          zadatci = transform(data)
       },
       error: function(data){
           alert("Greska")
       }
   })
   return zadatci
}


var getTaskUsers = async function(zadatak_id) {
    var korisnici = undefined
    await $.ajax({
       type: "GET",
       url: "/gettaskusers",
       data: {
           'zadatak': zadatak_id,
           'csrfmiddlewaretoken': token
       },
       success: function(data) {
          korisnici = transform(data)
       },
       error: function(data){
           alert("Greska")
       }
   })
   return korisnici
}

var getTask = async function(zadatak_id) {
    var zadatak = undefined
    await $.ajax({
       type: "GET",
       url: "/gettask",
       data: {
           'id': zadatak_id,
           'csrfmiddlewaretoken': token
       },
       success: function(data) {
          zadatak = transform(data)[0]
       },
       error: function(data){
           alert("Greska")
       }
   })
   return zadatak
}

var getPercentage = async function() {
    var postotak = undefined
    await $.ajax({
        type: "GET",
        url: "/getpercentage",
        data: {
            'csrfmiddlewaretoken': token
        },
        success: function(data) {
            postotak = data
        },
        error: function(data){
            alert("Greska")
        }
    })
    return postotak
}




