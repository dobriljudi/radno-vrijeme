
const formReset = function() {
    $("#activity-form, #login-form, #user-registration-form, #group-form, #task-form, #record-form").trigger("reset")
}

const add_button_listeners = function() {
    $(".group-list").on('click', '.group-selection', function() {
        if($(this).attr("class").includes("active")) {
            $('.edit-activity, .edit-task, .add-activity, .add-task, .add-worker, .delete-activity, .delete-task, .delete-worker').attr("disabled", true)
            $(this).attr("class", "group-selection list-group-item list-group-item-action");
            $('.group-display').hide()

            //Za stranicu upravljanje grupom
            $('.add-activity.group, .delete-activity.group').removeAttr("disabled")
            $('.delete-activity.group').hide()

        } else {
            $(".group-selection").each(function(i) {
                if($(this).attr("class").includes("active"))
                    $(this).attr("class", "group-selection list-group-item list-group-item-action");
            });

            $('.add-activity, .add-task, .add-worker').removeAttr("disabled")
            $(this).attr("class", "group-selection list-group-item list-group-item-action active");
            $('.group-display').show()
         
            //Za stranicu upravljanje grupom
            $('.add-activity.group, .delete-activity.group').removeAttr("disabled")
            $('.delete-activity.group').show()
        }
    })

    $('.group-activity-gallery').on('click', '.group-activity', function() {
        if($(this).attr("class").includes("active")) {
            $('.edit-activity, .edit-task, .delete-activity, .delete-task').attr("disabled", true)
            $(this).attr("class", "group-activity");
            $(".workers-display").hide()
        } else {
            $(".group-activity").each(function(i) {
                if($(this).attr("class").includes("active"))
                  $(this).attr("class", "group-activity");
            });

            $('.edit-activity, .edit-task, .delete-activity, .delete-task').removeAttr("disabled")
            $(this).attr("class", "group-activity active");
            $(".workers-display").show()
        }
    })

    //sluzi za odabir korisnika prilikom dodavanja u grupu oznaka in-group služi za razlikovanje dvije razlicite liste
    $('.coworkers-list').on('click', '.worker-selection', function() {
        if($(this).attr("class").includes("in-group")) {
            if($(this).attr("class").includes("active")) {
                $('.delete-worker').attr("disabled", true)
                $(this).attr("class", "worker-selection list-group-item list-group-item-action in-group")
                $(".user-display").hide()
            } else {
                
                $(".worker-selection").each(function(i) {
                    if($(this).attr("class").includes("active"))
                        $(this).attr("class", "worker-selection list-group-item list-group-item-action in-group");
                })

                $('.delete-worker').removeAttr("disabled", true)
                $(this).attr("class", "worker-selection list-group-item list-group-item-action active in-group");
                $(".user-display").show()
            }

        } else {

            if($(this).attr("class").includes("active")) {
                $('.add-new-activity-action').attr("disabled", true)
                $(this).attr("class", "worker-selection list-group-item list-group-item-action");
            } else {
                
                $(".worker-selection").each(function(i) {
                    if($(this).attr("class").includes("active"))
                        $(this).attr("class", "worker-selection list-group-item list-group-item-action");
                })

                $('.add-new-activity-action').removeAttr("disabled")
                $(this).attr("class", "worker-selection list-group-item list-group-item-action active");
            }


        }
    })

    $('.task-list').on('click', '.task-selection', function() {
        if($(this).attr("class").includes("active")) {
            $(this).attr("class", "task-selection list-group-item list-group-item-action");
            $('.task-display').hide()
        } else {
            $(".task-selection").each(function(i) {
                if($(this).attr("class").includes("active"))
                    $(this).attr("class", "task-selection list-group-item list-group-item-action");
            });
            $(this).attr("class", "task-selection list-group-item list-group-item-action active");      
            $('.task-display').show()
        }
    })

    $("#activity-form, #login-form, #user-registration-form, #group-form, #task-form, #record-form").on("click", function(event) {
        event.preventDefault()
    })

    $(".discard-add-new-activity-action").click(function() {
        formReset()
        $(".popup-window, .popup-window-second").hide()
    })
    
    $(".add-activity, .add-task, #login, .task-bar-btn").click(function() {
        $(".popup-window").show()
    })

    $(".add-worker").click(function() {
        $(".popup-window-second").show()
    })

    $(".edit-activity, .edit-task, .edit-profile").click(function() {
        $(".add-new-activity-action").text("Spremi promjene")
        $(".popup-window").show()
    })
}
