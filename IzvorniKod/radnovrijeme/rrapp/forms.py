from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm

from .models import Djelatnost, Grupa, Korisnik


class CreateUserForm(ModelForm):

    username = forms.CharField(max_length = 30)
    email = forms.EmailField()
    password = forms.CharField(widget = forms.PasswordInput)
    

    #Uzima polja iz modela Korisnik.
    class Meta:
        model = Korisnik
        exclude = ['korisnik', 'stanje', 'zadatci']

    """def __init__(self, *args, email, username):
        super(CreateUserForm, self).__init__(*args)
        self.email = email
        self.username = username"""
    
    
    #Provjera postoji li već registrirani korisnik sa upisanim emailom.
    def clean_email(self):
        email = self.cleaned_data.get('email')
        try:
            match = User.objects.get(email = email)
        except User.DoesNotExist:
            return email    
        raise forms.ValidationError('Korisnik sa ovom email adresom već postoji!')

    #Provjerava postoji li već registrirani korisnik sa upisanim usernameom.
    def clean_username(self):
        uName = self.cleaned_data.get('username')
        try:
            match = User.objects.get(username = uName)
        except User.DoesNotExist:
            return uName
        

        raise forms.ValidationError('Korisnik sa ovim usernameom već postoji!')
    


class AddActivityForm(ModelForm):
    
    #Postavi Idove i labele kako treba.
    def __init__(self, *args, **kwargs):
        super(AddActivityForm, self).__init__(*args, **kwargs)
        self.fields['ime_djelatnosti']=forms.CharField(widget=forms.TextInput(attrs={'id':'ime'}), label='Ime')
        self.fields['opis_djelatnosti']=forms.CharField(widget=forms.TextInput(attrs={'id':'opis'}), label='Opis')
        self.fields['satnica_kn']=forms.IntegerField(widget=forms.NumberInput(attrs={'id':'satnica'}), label='Satnica')


    class Meta:
        model = Djelatnost
        exclude = ['grupa']

#Form za upisivanje imena grupe pri kreaciji.
class GroupNameForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(GroupNameForm, self).__init__(*args, **kwargs)
        self.fields['ime_grupe']=forms.CharField(widget=forms.TextInput(attrs={'id':'imegrupe', 'name': 'ime_grupe'}), label='Ime grupe')
        
    class Meta:
        model = Grupa
        fields = ['ime_grupe']


#Form za izlistavanje usera.
class ListUserForm(forms.Form):
    #voditelj = forms.ChoiceField(required=True)
    
    #Usernames je lista imena svih korsnika, stavljeno je da se može form koristiti uz uvijete.
    def __init__(self, *args, usernames, lbl):
        super().__init__(*args)
        
        leaders = [(i.id, i.username) for i in usernames]
        self.fields[lbl.lower()] = forms.ChoiceField(choices=leaders, label = lbl)

    
#Form za izlistavanje svih grupa.
class ListGroupsForm(ModelForm):

    def __init__(self, *args, id):
        super(ListGroupsForm, self).__init__(*args)
        groups = Grupa.objects.all()
        groupNames = [(i.id, i.ime_grupe) for i in groups]
        self.fields['ime_grupe'] = forms.ChoiceField(widget = forms.Select(attrs = {'id' : id}), choices=groupNames)

    class Meta:
        model = Grupa
        fields = ['ime_grupe']



     