from django.db import models
from django.contrib.auth.models import User

#Note: Unique ID je implicitno dodan u svaki model u Djangu

#Django ima ugradenu podrsku za korisnike, passworde, login itd
#Umjesto da radimo svoju podrsku za login kako bi dodali dodatne podatke u tablicu
#Konvencija je svoju Korisnik tablicu povezati OneToOne s ugradenom Django User tablicom
class Korisnik(models.Model):
    korisnik = models.OneToOneField(User, on_delete=models.CASCADE)
    username = models.CharField(max_length=30)
    ime = models.CharField(max_length=30)
    prezime = models.CharField(max_length=30)

    #Postoje 3 opcije koje idu u polje uloga
    ULOGE = (
        ('D', 'Direktor'),
        ('V', 'Voditelj'),
        ('R', 'Radnik')
    )
    uloga = models.CharField(max_length=1, choices=ULOGE)
    kontakt = models.CharField(max_length=30, blank=True) #blank=True postavlja polje kao opcionalno (O) tj. moze biti blank

    STANJA = (
        ('S', 'Slobodan'),
        ('Z', 'Zauzet')
    )
    stanje = models.CharField(max_length=1, choices=STANJA, default='S')

    #path na sliku profila na serveru. Nije jos odluceno kako cemo spremati slike. TODO
    slika_path = models.CharField(max_length=100, blank=True) 

    #Many-To-Many s intermadiate modelom Ima_zadatak
    zadatci = models.ManyToManyField('Zadatak', through='Ima_zadatak')

    #Dodavanje permission objekta u metapodatke ove tablice
    class Meta:
        permissions = [("can_modify_users", "Can modify users")]

class Grupa(models.Model):
    ime_grupe = models.CharField(max_length=30)

    #TODO provjeri jeli treba biti identifying
    voditelj = models.OneToOneField('Korisnik', related_name='voditelj', on_delete=models.CASCADE) #Isto kao foreign key samo ima constraint "unique" 

    #Ovo bi trebalo biti ekvivalentno tablici 'pripada' u REL modelu
    clanovi = models.ManyToManyField('Korisnik', blank=True)
    
    class Meta:
        permissions = [("can_modify_groups", "Can modify groups")]


class Djelatnost(models.Model):
    ime_djelatnosti = models.CharField(max_length=30)
    opis_djelatnosti = models.CharField(max_length=100)
    satnica_kn = models.IntegerField()

    STANJA = (
        ('A', 'Aktivan'),
        ('Z', 'Zavrsen'),
        ('O', 'Otkazan')
    )
    stanje = models.CharField(max_length=1, choices=STANJA, default='A')

    #Many-to-one. Djelatnost ima jednu grupu, grupa ima vise djelatnosti
    grupa = models.ForeignKey(Grupa, on_delete=models.CASCADE, blank=True) 

class Zadatak(models.Model):
    ime = models.CharField(max_length=30)
    opis = models.CharField(max_length=100)
    satnica = models.IntegerField(blank=True)
    predvideno_trajanje = models.IntegerField() #broj sati

    STANJA = (
        ('A', 'Aktivan'),
        ('Z', 'Zavrsen'),
        ('O', 'Otkazan')
    )
    stanje = models.CharField(max_length=1, choices=STANJA, default='A')

    #Many-to-one. Zadatak je u jednoj djelatnosti, djlatnost ima vise zadataka 
    djelatnost = models.ForeignKey(Djelatnost, on_delete=models.CASCADE)

    class Meta:
        permissions = [("can_modify_tasks", "Can modify tasks")]

#Ovo se zove Intermediate model. Potreban je ako hocemo pripisati dodatne podatke (trajanje_zadatka) Many-to-Many odnosu
class Ima_zadatak(models.Model):
    korisnik = models.ForeignKey(Korisnik, on_delete=models.CASCADE)
    zadatak = models.ForeignKey(Zadatak, on_delete=models.CASCADE)

    trajanje_zadatka = models.IntegerField()

#Evidencija zadatka je postavljena kao zasebna tablica (ne ovisi o postojanju korisnika/zadatka)
#TODO provjeri jeli bitno postaviti vezu Korisnik-Evidencija kao identifying ili je ovo ok.
class Evidencija(models.Model):
    korisnik = models.ForeignKey(Korisnik, on_delete=models.CASCADE)
    zadatak = models.ForeignKey(Zadatak, on_delete=models.CASCADE)

    timestamp = models.DateTimeField(auto_now=True)
    satnica = models.IntegerField()

    opis = models.CharField(max_length=500)
    

    