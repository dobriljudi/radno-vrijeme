# Generated by Django 3.1.4 on 2021-01-05 19:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rrapp', '0006_auto_20210105_1623'),
    ]

    operations = [
        migrations.AddField(
            model_name='korisnik',
            name='stanje',
            field=models.CharField(choices=[('S', 'Slobodan'), ('Z', 'Zauzet')], default='S', max_length=1),
        ),
        migrations.AddField(
            model_name='zadatak',
            name='stanje',
            field=models.CharField(choices=[('A', 'Aktivan'), ('Z', 'Zavrsen'), ('O', 'Otkazan')], default='A', max_length=1),
        ),
    ]
